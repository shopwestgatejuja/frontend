import "../styles/globals.css";
import "antd/dist/antd.css";

import { createClient, Provider } from "urql";
import NextNProgress from "nextjs-progressbar";
import { createContext, useState } from "react";

export const AuthContext = createContext();

export const client = createClient({
  url: "https://westgate-shop.herokuapp.com/graphql",
});

function MyApp({ Component, pageProps }) {
  const [customer, setCustomer] = useState(null);

  return (
    <Provider value={client}>
      <AuthContext.Provider value={[customer, setCustomer]}>
        <NextNProgress color="#3F9B42" />
        <Component {...pageProps} />
      </AuthContext.Provider>
    </Provider>
  );
}

export default MyApp;
