import { useState, useEffect, useRef, useContext } from "react";
import { useQuery, useClient } from "urql";
import { AuthContext } from "./_app";
import { client } from "../helpers/algolia-search.js";
import {
  InstantSearch,
  Configure,
  RefinementList,
  connectRefinementList,
  connectInfiniteHits,
  connectSearchBox,
  connectHits,
} from "react-instantsearch-dom";

import { Product, Title, Category } from "../components";

import { Badge, Avatar, Input, Tag } from "antd";
import { ShoppingOutlined, UserOutlined } from "@ant-design/icons/lib/icons";

import { useRouter } from "next/router";
import { FaWhatsapp } from "react-icons/fa";

const { Search } = Input;

export default function Home() {
  // Router & refs
  const router = useRouter();
  const [customer, setCustomer] = useContext(AuthContext);

  useEffect(() => {
    if (localStorage.getItem("customer") && !customer) {
      let _customer = localStorage.getItem("customer");
      let _customer_ = JSON.parse(_customer);
      setCustomer(_customer_);
    }
  }, []);

  // Requests
  const GET_CUSTOMER = `
      query GET_CUSTOMER($id:ID!){
        getCustomer(id:$id){
          id
          cart{
            id
            products{
              meta{
                id             
              }
              quantity
            }
          }
        }
      }
    `;

  const [{ data, fetching, error }, reexecuteQuery] = useQuery({
    query: GET_CUSTOMER,
    variables: {
      id: customer?.id,
    },
  });

  //States
  const [cartCount, setCartCount] = useState(0);
  const [loadingMore, setLoadingMore] = useState(false);
  const [suggestions, setSuggestions] = useState([]);
  const [loadingSearch, setLoadingSearch] = useState(false);
  const [keyword, setKeyword] = useState("");
  const [_category, setCategory] = useState("");

  // Functions

  const SearchBox = ({ currentRefinement, isSearchStalled, refine }) => (
    <Search
      allowClear
      placeholder="Ex. Egg, sugar"
      className="max-w-[60%] lg:max-w-[40%]"
      size="large"
      value={currentRefinement}
      onChange={(event) => {
        refine(event.currentTarget.value);
      }}
    />
  );

  const CustomSearchBox = connectSearchBox(SearchBox);

  const Hits = ({
    hits,
    hasPrevious,
    refinePrevious,
    hasMore,
    refineNext,
    insights,
  }) => {
    const fetchMore = () => {
      if (
        scrollDiv.current.offsetHeight + scrollDiv.current.scrollTop >=
          scrollDiv.current.scrollHeight - 100 &&
        hasMore
      ) {
        refineNext();
      }
    };

    const scrollDiv = useRef();

    return (
      <div
        ref={scrollDiv}
        onScroll={fetchMore}
        className="px-3 max-h-[1000px] relative gap-4 grid grid-cols-2 sm:grid-cols-3 md:grid-cols-4 lg:grid-cols-5  max-h-[calc(100vh - 180px)] overflow-y-auto"
      >
        {hits
          .filter((hit) => {
            if (!_category) {
              return !hit.removed;
            } else {
              return !hit.removed && hit.category == _category;
            }
          })
          .map((hit, i) => (
            <Product
              key={i}
              customer={data?.getCustomer}
              hit={hit}
              error={error}
              refresh={reexecuteQuery}
            />
          ))}
      </div>
    );
  };

  const CustomHits = connectInfiniteHits(Hits);

  const Results = ({
    hits,
    hasPrevious,
    refinePrevious,
    hasMore,
    refineNext,
  }) => {
    return (
      <div className="flex mt-[72px] space-x-4 max-w-full  overflow-y-auto">
        {hits
          .filter((hit) => !hit.removed)
          .map((hit, i) => (
            <Product
              key={i}
              customer={data?.getCustomer}
              hit={hit}
              error={error}
              refresh={reexecuteQuery}
            />
          ))}
      </div>
    );
  };

  const CustomResults = connectHits(Results);

  const RefinementList = ({ items, currentRefinement, refine }) => {
    return (
      <>
        {currentRefinement.length > 0 && (
          <p className="text-sm text-gray-500">
            Showing:{" "}
            {currentRefinement.map((_refinement, i) => (
              <Tag key={i} color="green">
                {_refinement.charAt(0).toUpperCase() +
                  _refinement.substring(1, _refinement.length)}
              </Tag>
            ))}
          </p>
        )}
        <div className="flex flex-wrap w-full">
          {items.map((item, inx) => (
            <Category
              key={inx}
              onSelect={() => {
                currentRefinement = null;
                refine(item.value);
              }}
              active={item.isRefined ? true : false}
              icon={
                item.label == "sodas"
                  ? "/categories/soda.png"
                  : item.label == "juices"
                  ? "/categories/soda.png"
                  : item.label == "drinks"
                  ? "/categories/drinks.png"
                  : item.label == "medicine"
                  ? "/categories/medicine.png"
                  : item.label == "snacks"
                  ? "/categories/snacks.png"
                  : item.label == "legumes & flour"
                  ? "/categories/flour.png"
                  : item.label == "breads & cakes"
                  ? "/categories/bread.png"
                  : item.label == "spices & seasoning"
                  ? "/categories/seasoning.png"
                  : item.label == "plastics"
                  ? "/categories/plastics.png"
                  : item.label == "ingredients"
                  ? "/categories/ingredients.png"
                  : item.label == "milk products"
                  ? "/categories/milk.png"
                  : item.label == "meat products"
                  ? "/categories/meat.png"
                  : item.label == "frozen food"
                  ? "/categories/frozen.png"
                  : item.label == "electricals & electronics"
                  ? "/categories/electronics.png"
                  : item.label == "clothing"
                  ? "/categories/clothing.png"
                  : item.label == "dental products"
                  ? "/categories/dental.png"
                  : item.label == "gas & gas products"
                  ? "/categories/gas.png"
                  : item.label == "stationary"
                  ? "/categories/stationery.png"
                  : item.label == "fruits & vegetables"
                  ? "/categories/fruits.png"
                  : item.label == "oil & skin products"
                  ? "/categories/skincare.png"
                  : item.label == "toiletries"
                  ? "/categories/toiletries.png"
                  : item.label == "cleaning"
                  ? "/categories/cleaning.png"
                  : item.label == "soaps & cleaning"
                  ? "/categories/soaps.png"
                  : "/categories/hardware.png"
              }
              rounded
              label={
                item.label.charAt(0).toUpperCase() +
                item.label.substring(1, item.label.length)
              }
            />
          ))}
        </div>
      </>
    );
  };

  const CustomRefinementList = connectRefinementList(RefinementList);

  return (
    <div className="relative">
      <div className="pl-4">
        {/* Header */}
        <InstantSearch searchClient={client} indexName="westgate-products">
          <>
            {/* Search box */}
            <div className="flex w-full justify-between py-3 pr-8 pt-5 fixed bg-white top-0 z-30">
              <CustomSearchBox />

              <div className="flex space-x-4 max-w-">
                <button
                  onClick={() =>
                    router.push("https://wa.me/254733124161?text=")
                  }
                >
                  <FaWhatsapp size={24} color="green" />
                </button>
                <button
                  onClick={() => {
                    // Go to login if not signed in or go to cart
                    if (customer) {
                      router.push("/basket");
                    } else {
                      router.push("/login");
                    }
                  }}
                >
                  <Badge
                    color="#3F9B42"
                    count={data?.getCustomer?.cart?.products.length || null}
                  >
                    <ShoppingOutlined
                      style={{ fontSize: "1.5rem", color: "#707070" }}
                    />
                  </Badge>
                </button>
                <button
                  onClick={() => {
                    // Go to login if not signed in or go to account
                    if (customer) {
                      router.push("/account");
                    } else {
                      router.push("/login");
                    }
                  }}
                >
                  <Avatar
                    style={{ backgroundColor: "#3F9B42" }}
                    icon={<UserOutlined />}
                  />
                </button>
              </div>
            </div>

            {/* Results */}
            <CustomResults />
            <br />
          </>
        </InstantSearch>

        <InstantSearch searchClient={client} indexName="westgate-products">
          {/* Categories */}
          <div>
            <Title text="Categories" />
            <br />

            <CustomRefinementList attribute="category" limit={30} />
            <br />
          </div>

          {/* Recommended */}
          <Title text="Recommended" />
          <br />
          <Configure hitsPerPage={60} analytics={false} />
          <CustomHits />
        </InstantSearch>
      </div>
    </div>
  );
}

// Functions & Components
export const ProductList = ({
  customer,
  getCartCount,
  category,
  refresh,
  myRef,
}) => {
  // Setups
  const graphqlClient = useClient();
  const [prods, setProds] = useState([]);
  const [pageInfo, setPageInfo] = useState(null);

  // Requests
  const GET_PRODUCTS = `
      query GET_PRODUCTS( $afterCursor: String , $category: String){
        getProducts(first:20, afterCursor:$afterCursor , category: $category){
          edges {
            cursor
            node {  
              id
              category
              sale_end
              sale_start
              image
              name
              offer_price
              price
              quantity  
              removed
              outstocked
            }
          }
          totalCount
        pageInfo{
          startCursor
          hasNextPage
        }
        }
      }
    `;

  if (customer) {
    getCartCount(customer?.cart?.products.length || 0);
  }

  useEffect(() => {
    loadData();
  }, []);

  const loadData = () => {
    if (pageInfo && !pageInfo?.hasNextPage) {
      return;
    }
    graphqlClient
      .query(GET_PRODUCTS, {
        first: 20,
        afterCursor: pageInfo ? pageInfo.startCursor : null,
        removed: false,
        category,
      })
      .toPromise()
      .then(({ data }) => {
        setProds((prods) => [
          ...prods,
          ...data?.getProducts.edges.filter((edge) => {
            return edge;
          }),
        ]);
        setPageInfo(data?.getProducts.pageInfo);
      });
  };

  myRef.current.loadData = loadData;

  return (
    <>
      <div>
        <div style={{ display: "flex", flexWrap: "wrap" }}>
          {prods.map((edge, i) => {
            return (
              <Product
                key={i}
                data={edge}
                customer={customer}
                error={customer ? false : true}
                refresh={refresh}
              />
            );
          })}
        </div>
      </div>
    </>
  );
};
