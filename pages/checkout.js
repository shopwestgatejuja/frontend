import { useState, useContext, useEffect } from "react";
import { useQuery, useMutation } from "urql";
import { Page, Container } from "../components";
import {
  Steps,
  Card,
  Radio,
  Space,
  Tag,
  Button as ButtonAntd,
  Typography,
  message,
  Tooltip,
  Modal,
  Result,
} from "antd";
import { useRouter } from "next/router";
import { InfoCircleFilled } from "@ant-design/icons";
import { AuthContext } from "./_app";
import axios from "axios";
import { io } from "socket.io-client";

const { Step } = Steps;
const { Text } = Typography;

export default function Checkout() {
  // States & Hooks
  const [customer, setCustomer] = useContext(AuthContext);
  const [loadingOrdering, setLoadingOrdering] = useState(false);
  const [paymentMethod, setPaymentMethod] = useState("mpesa-now");
  const [deliver, setDeliver] = useState(true);
  const [infoModal, setInfoModal] = useState({ value: false, data: null });
  const [sucessModal, setSucessModal] = useState(false);
  const [current, setCurrent] = useState(0);
  const router = useRouter();

  const socket = io("https://tinypesa-wrapper.herokuapp.com");

  useEffect(() => {
    if (localStorage.getItem("customer") && !customer) {
      let _customer = localStorage.getItem("customer");
      let _customer_ = JSON.parse(_customer);
      setCustomer(_customer_);
      return;
    } else if (!localStorage.getItem("customer") && !customer) {
      router.push("/login");
    }
  }, []);

  // Requests
  const GET_CUSTOMER = `
  query GET_CUSTOMER($id:ID!){
    getCustomer(id:$id){
      id
      displayName
      phoneNumber
      location
      cart{
        id
        products{
          meta{
            id
            sale_start
            quantity
            image
            offer_price
            name
            price
          }
          quantity
        }
      }
      old{
        id
        products{
          meta{
            id
            name
            price
          }
          quantity
        }
      }
      packed{
        id
        products{
          meta{
            id
            name
            price
          }
          quantity
        }
      }
      awaitingPacking{
        id
        products{
          meta{
            id
            name
            price
          }
          quantity
        }
      }
    }
  }`;

  const [{ data: cData, fetching: cFetching, error: cError }, reexecuteQuery] =
    useQuery({
      query: GET_CUSTOMER,
      variables: {
        id: customer?.id,
      },
    });

  const _customer = cData?.getCustomer;

  const UPDATE_ORDER = `
    mutation UPDATE_ORDER(
      $id:ID!
      $status: String
      $deliver: Boolean  
      $ref: String
      $timestamp: String
      $amount: String
    ){
      updateOrder(
        id:$id
        status:$status
        deliver:$deliver
        ref:$ref
        timestamp:$timestamp
        amount:$amount
      )
      {
        id
        status
      }
    }
  `;

  const [mResult, _updateOrder] = useMutation(UPDATE_ORDER);

  // Functions
  const getTotal = () => {
    let tot = 0;
    cData?.getCustomer.cart?.products.forEach((product) => {
      tot += product.meta.price * product.quantity;
    });
    return tot;
  };

  const completeOrder = async () => {
    if (!_customer.phoneNumber) {
      message.warn("Missing phone number");
      return;
    }

    if (deliver && !_customer.location) {
      message.warn("Mising delivery location");
      return;
    }

    //  Intitiate stk
    setLoadingOrdering(true);
    axios.post("https://tinypesa-wrapper.herokuapp.com/send", {
      ApiKey: process.env.API_KEY,
      amount: deliver ? 50 + getTotal() : getTotal(),
      msisdn: _customer.phoneNumber,
      account_no: "WESTGATE_SHOP",
    });

    // Listen for events
    socket.on("payload", (data) => {
      const { stkCallback } = data;
      const { CallbackMetadata } = stkCallback;

      console.log(CallbackMetadata);

      switch (stkCallback.ResultCode) {
        case 1032:
          setLoadingOrdering(false);
          setInfoModal({ value: true, code: 1032 });
          break;

        case 0:
          setLoadingOrdering(false);
          setSucessModal(true);

          let payload = {
            id: _customer?.cart?.id,
            status: "in_processing",
            deliver,
            ref: CallbackMetadata.Item[1].Value,
            timestamp: Date.now().toString(),
            amount: CallbackMetadata.Item[0].Value.toString(),
          };

          _updateOrder(payload)
            .then(({ data }) => {
              console.log(data);
              if (data) {
                router.push("/");
              } else {
                console.log("error");
              }
            })
            .catch((err) => console.log(err));
          break;

        case 17:
          setLoadingOrdering(false);
          setInfoModal({ value: true, code: 17 });
          break;

        default:
          setLoadingOrdering(false);
          // setInfoModal({ value: true, code: stkCallback.ResultCode })
          break;
      }
    });
  };

  const Delivery = () => {
    return (
      <div style={{ width: "100%" }}>
        <br />
        <Card size="small" title="Address details" style={{ width: "100%" }}>
          <div style={{ color: "#707070", padding: 8 }}>
            <h3>{_customer?.displayName}</h3>
            <p style={{ margin: 0 }}>{_customer?.phoneNumber}</p>
            <p style={{ color: "#3F9B42", margin: 0 }}>{_customer?.location}</p>
          </div>
          <div
            style={{
              padding: 8,
              display: "flex",
              width: "100%",
              position: "relative",
              justifyContent: "space-between",
            }}
          >
            {!_customer?.location && deliver && (
              <div>
                <Text
                  type="warning"
                  style={{
                    display: "block",
                    margin: "16px 0px",
                    width: "100%",
                  }}
                >
                  <InfoCircleFilled /> Missing delivery point{" "}
                </Text>
                <ButtonAntd
                  type="ghost"
                  block
                  style={{ width: "100%", marginBottom: 16 }}
                  onClick={() => router.push("/account")}
                >
                  Edit in account
                </ButtonAntd>
              </div>
            )}
            {!_customer?.phoneNumber && (
              <>
                <Text
                  type="warning"
                  style={{
                    display: "block",
                    margin: "0px",
                    width: "100%",
                  }}
                >
                  <InfoCircleFilled /> Missing phone number{" "}
                  <ButtonAntd
                    type="link"
                    style={{ float: "right", marginTop: -4 }}
                    onClick={() => router.push("/account")}
                  >
                    Add
                  </ButtonAntd>
                </Text>
              </>
            )}
          </div>
        </Card>
        <Radio.Group
          style={{ marginTop: 12 }}
          onChange={(e) => {
            if (e.target.value == "collect") {
              setDeliver(false);
              return;
            } else {
              setDeliver(true);
            }
          }}
          value={deliver ? "deliver" : "collect"}
        >
          <Space direction="vertical">
            <Radio value={"deliver"}>Deliver</Radio>
            <Radio value={"collect"}>Collect</Radio>
          </Space>
        </Radio.Group>
      </div>
    );
  };

  const Payment = () => {
    return (
      <div style={{ padding: "16px 0px" }}>
        <Radio.Group onChange={null} value={"mpesa-now"}>
          <Space direction="vertical">
            <Radio value={"mpesa-now"}>Pay now - M-pesa</Radio>
            <Radio value={"on-delivery"} disabled>
              Pay on delivery
            </Radio>
          </Space>
        </Radio.Group>
      </div>
    );
  };

  const Summary = () => {
    return (
      <div>
        <br />
        <Card
          size="small"
          title="Total"
          extra={<span style={{ color: "#3F9B42" }}>Ksh. {getTotal()}</span>}
          style={{ width: "100%", marginBottom: "2rem" }}
        >
          <div style={{ color: "#707070", padding: 8 }}>
            {_customer?.cart?.products.map((cartItem, index) => {
              return (
                <div
                  style={{
                    borderBottom: "#f1f1f1 1px dashed",
                    display: "flex",
                    justifyContent: "space-between",
                    padding: "8px 0px",
                  }}
                  key={index}
                >
                  <Tooltip
                    title={cartItem.meta.name}
                    color="#3F9B42"
                    trigger="click"
                  >
                    <Text ellipsis style={{ width: 120 }}>
                      {cartItem.meta.name}
                    </Text>
                  </Tooltip>

                  <Tag color="green" style={{ height: 24, width: 24 }}>
                    {cartItem.quantity}
                  </Tag>
                  <span>@</span>
                  <span>{cartItem.meta.price}</span>
                  <span>{cartItem.quantity * cartItem.meta.price}</span>
                </div>
              );
            })}
          </div>
          <br />
          {deliver && (
            <div style={{ padding: 8, marginBottom: 6 }}>
              Delivery + Bag : 50
            </div>
          )}

          <div style={{ padding: 8, marginBottom: 6 }}>
            GRAND TOTAL :{" "}
            <h2 style={{ color: "green", display: "inline" }}>
              Ksh. {deliver ? 50 + getTotal() : getTotal()}
            </h2>
          </div>
          <span style={{ padding: 8 }}>
            Payment method :{" "}
            <Tag color="green">
              {paymentMethod == "mpesa-now" ? "MPESA NOW" : "PAY ON DELIVERY"}
            </Tag>
          </span>
        </Card>
      </div>
    );
  };

  return (
    <Page title="Checkout">
      <Container
        style={{
          position: "absolute",
          top: 56,
          width: "calc(100% - 16px)",
          paddingBottom: "8rem",
        }}
      >
        <br />
        <Steps direction="vertical" size="small" current={current}>
          <Step title="Address" description={<Delivery />} />
          <Step title="Payment" description={<Payment />} />
          <Step title="Summary" description={<Summary />} />
        </Steps>
        <ButtonAntd
          loading={loadingOrdering}
          onClick={completeOrder}
          style={{
            marginBottom: "2rem",
            marginLeft: "5%",
            width: "90%",
            fontFamily: "Metropolis-Regular",
            color: "#3F9B42",
            display: "block",
            fontSize: "1.1rem",
            padding: "0.9rem",
            backgroundColor: "#E5E055",
            fontWeight: "900",
            height: 50,
            textTransform: "uppercase",
            color: "#fff",
            border: "none",
            borderRadius: "8px",
          }}
        >
          Complete order
        </ButtonAntd>
      </Container>

      <Modal
        visible={sucessModal}
        footer={null}
        onCancel={() => setSucessModal(false)}
      >
        <Result
          status="success"
          title="Order complete!"
          subTitle="You can track your order in the basket page."
        />
      </Modal>

      <Modal
        visible={infoModal.value}
        footer={null}
        onCancel={() => setInfoModal({ value: false, data: null })}
      >
        {infoModal.data == 1032 ? (
          <Result
            status="error"
            title="Transaction cancelled!"
            subTitle="Looks like you cancelled the transaction. Order not complete"
          />
        ) : infoModal.data == 17 ? (
          <Result
            status="info"
            title="Another transaction underway!"
            subTitle="Another transaction was underway. Please wait shortly and try completing the order again"
          />
        ) : (
          <Result
            status="info"
            title="Connectivity"
            subTitle={
              "Check your internet connection. Transaction not activated"
            }
          />
        )}
      </Modal>
    </Page>
  );
}
