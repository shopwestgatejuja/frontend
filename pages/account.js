import { Page, Container, Button } from "../components";
import { useRouter } from "next/router";
import { useState, useEffect } from "react";
import { Form, message, Typography } from "antd";
import { InfoCircleOutlined } from "@ant-design/icons/lib/icons";
import { useContext } from "react";
import { AuthContext } from "./_app";
import { useQuery, useMutation } from "urql";
import Login from "./login";

const { Paragraph } = Typography;

export default function AccountPage() {
  // Router , refs , context
  const router = useRouter();
  const [customer, setCustomer] = useContext(AuthContext);

  useEffect(() => {
    if (localStorage.getItem("customer") && !customer) {
      let _customer = localStorage.getItem("customer");
      let _customer_ = JSON.parse(_customer);
      setCustomer(_customer_);
      return;
    } else if (!localStorage.getItem("customer") && !customer) {
      router.push("/login");
    }
  }, []);

  const Account = () => {
    // States
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [location, setLocation] = useState("");
    const [phoneNumber, setPhoneNumber] = useState("");

    // Requests
    const GET_CUSTOMER = `
    query GET_CUSTOMER($id:ID!){
      getCustomer(id:$id){
        id
        displayName
        phoneNumber    
        email
        location
      }
    }
  `;

    const [
      { data: cData, fetching: cFetching, error: cError },
      reexecuteQuery,
    ] = useQuery({
      query: GET_CUSTOMER,
      variables: {
        id: customer?.id,
      },
    });

    const UPDATE_CUSTOMER = `
  mutation UPDATE_CUSTOMER(
    $id: ID! ,  
    $displayName: String,
    $email: String,
    $location: String,
    $phoneNumber: String,
    $photo: String
  ){
    updateCustomer(id:$id, displayName:$displayName , email:$email, location:$location, phoneNumber:$phoneNumber, photo:$photo){
      id
      displayName
      phoneNumber    
      email
      location
    }
  }`;

    const [updateCustomerResult, _updateCustomer] =
      useMutation(UPDATE_CUSTOMER);

    // Functions
    const Logout = () => {
      return (
        <button
          onClick={() => {
            setCustomer(null);
            localStorage.clear();
            message.success("Successfully logged out");
          }}
          style={{
            border: "none",
            outline: "none",
            background: "transparent",
            color: "#E5E055",
            fontSize: "0.9rem",
            zIndex: 4,
          }}
        >
          Sign out
        </button>
      );
    };

    const updateProfile = () => {
      let updates = {};
      if (name) {
        updates.displayName = name;
      }
      if (email) {
        updates.email = email;
      }
      if (location) {
        updates.location = location;
      }
      if (phoneNumber) {
        updates.phoneNumber = phoneNumber;
      }

      _updateCustomer({
        ...updates,
        id: customer.id,
      }).then(() => {
        setName(null);
        setEmail(null);
        setLocation(null);
        setPhoneNumber(null);
        reexecuteQuery({ requestPolicy: "network-only" });
        message.success("Profile updated");
      });
    };

    if (cFetching) return <p>Loading ....</p>;
    if (cError) return <p>Oh nooo ..error</p>;

    return (
      <Page title="Account" extra={<Logout />}>
        <Container style={{ position: "absolute", top: 56, width: "95%" }}>
          <br />

          <Form style={{ marginBottom: "7rem" }}>
            <Form.Item label="Name" name="name">
              <div style={{ padding: "4px 0px" }}>
                <input
                  style={{
                    background: "#f1f1f1",
                    width: "100%",
                    padding: "14px",
                    border: "none",
                    outline: "none",
                    borderRadius: 12,
                    color: name ? "#808080" : "#3F9B42",
                    fontWeight: "bold",
                    fontSize: "1.1rem",
                  }}
                  type="text"
                  value={name ? name : cData?.getCustomer?.displayName}
                  onChange={(e) => setName(e.target.value)}
                />
              </div>
            </Form.Item>
            <Form.Item label="Phone number" name="telephone">
              <div style={{ padding: "4px 0px" }}>
                <input
                  style={{
                    background: "#f1f1f1",
                    width: "100%",
                    padding: "14px",
                    border: "none",
                    outline: "none",
                    borderRadius: 12,
                    color: phoneNumber ? "#808080" : "#3F9B42",
                    fontWeight: "bold",
                    fontSize: "1.1rem",
                  }}
                  type="number"
                  value={
                    phoneNumber
                      ? phoneNumber
                      : !cData?.getCustomer?.phoneNumber
                      ? ""
                      : cData?.getCustomer?.phoneNumber
                  }
                  onChange={(e) => setPhoneNumber(e.target.value)}
                />
              </div>
            </Form.Item>
            <Form.Item label="Email" name="email">
              <div style={{ padding: "4px 0px" }}>
                <input
                  style={{
                    background: "#f1f1f1",
                    width: "100%",
                    padding: "14px",
                    border: "none",
                    outline: "none",
                    borderRadius: 12,
                    color: email ? "#808080" : "#3F9B42",
                    fontWeight: "bold",
                    fontSize: "1.1rem",
                  }}
                  type="text"
                  value={
                    email
                      ? email
                      : cData?.getCustomer?.email
                      ? cData?.getCustomer?.email
                      : ""
                  }
                  onChange={(e) => setEmail(e.target.value)}
                />
              </div>
            </Form.Item>
            <span>
              <InfoCircleOutlined style={{ marginRight: 8, color: "green" }} />
              <Paragraph
                style={{
                  display: "inline",
                  fontSize: "0.8rem",
                  color: "green",
                }}
              >
                * Building name, house no and GATE A/B/C .
              </Paragraph>
            </span>
            <Form.Item
              label="Location"
              name="address"
              style={{ marginTop: 16 }}
            >
              <div style={{ padding: "4px 0px" }}>
                <input
                  style={{
                    background: "#f1f1f1",
                    width: "100%",
                    padding: "14px",
                    border: "none",
                    outline: "none",
                    borderRadius: 12,
                    color: location ? "#808080" : "#3F9B42",
                    fontWeight: "bold",
                    fontSize: "1.1rem",
                  }}
                  type="text"
                  value={
                    location
                      ? location
                      : cData?.getCustomer?.location
                      ? cData?.getCustomer?.location
                      : ""
                  }
                  onChange={(e) => setLocation(e.target.value)}
                />
              </div>
            </Form.Item>
          </Form>
          <Button
            rounded
            label="Update"
            yellow
            width="90%"
            large
            style={{ position: "fixed", bottom: "3rem" }}
            onClick={updateProfile}
          />
        </Container>
      </Page>
    );
  };

  if (!customer) return <Login />;

  return <Account />;
}
