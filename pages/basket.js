import {
  Container,
  Product,
  Button,
  Flex,
  Page,
  Title,
  SkeletonCustom,
} from "../components";
import {
  Button as ButtonAntd,
  Divider,
  Tooltip,
  Typography,
  Image,
} from "antd";
import { useRouter } from "next/router";
import { AuthContext } from "./_app";
import { useContext, useEffect } from "react";
import { useQuery } from "urql";
import moment from "moment";

const { Paragraph } = Typography;

export default function Basket() {
  // Hooks
  const router = useRouter();
  const [customer, setCustomer] = useContext(AuthContext);

  useEffect(() => {
    if (localStorage.getItem("customer") && !customer) {
      let _customer = localStorage.getItem("customer");
      let _customer_ = JSON.parse(_customer);
      setCustomer(_customer_);
      return;
    } else if (!localStorage.getItem("customer") && !customer) {
      router.push("/login");
    }
  }, []);

  // Requests
  const GET_CUSTOMER = `
  query GET_CUSTOMER($id:ID!){
    getCustomer(id:$id){
      id
      cart{
        id
        products{
          meta{
            id
            sale_start
            quantity
            image
            offer_price
            name
            price
          }
          quantity
        }
      }
      old{
        id
        products{
          meta{
            id
            name
            image
            price
          }
          quantity
        }
        disburseDate
      }
      packed{
        id
        products{
          meta{
            id
            name
            image
            price
          }
          quantity
        }
        disburseDate
      }
      awaitingPacking{
        id
        products{
          meta{
            id
            name
            image
            price
          }
          quantity
        }
        disburseDate
      }
    }
  }`;

  const [{ data: cData, fetching: cFetching, error: cError }, reexecuteQuery] =
    useQuery({
      query: GET_CUSTOMER,
      variables: {
        id: customer?.id,
      },
    });

  // Functions

  const Bill = ({ total }) => {
    return (
      <p
        style={{
          color: "rgb(229, 224, 85)",
          lineHeight: 2.5,
          fontFamily: "Metropolis-Regular",
        }}
      >
        {total}
      </p>
    );
  };

  const getTotal = () => {
    let tot = 0;
    cData?.getCustomer?.cart?.products.forEach((product) => {
      tot += product?.meta?.price * product?.quantity;
    });
    return tot;
  };

  return (
    <Page title="Basket" extra={<Bill total={`Ksh. ${getTotal()}`} />}>
      <Container style={{ position: "absolute", top: 56, minWidth: "95vw" }}>
        {cError && <p>Error...</p>}

        {cFetching && (
          <Flex scrollY style={{ paddingBottom: "5rem" }}>
            {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map((el) => (
              <SkeletonCustom key={el} />
            ))}
          </Flex>
        )}

        {cData?.getCustomer?.cart?.products.length > 0 && (
          <div style={{ maxHeight: "calc(100vh - 170px)" }}>
            <div className="px-3 max-h-[1000px] relative gap-4 grid grid-cols-2 sm:grid-cols-3 md:grid-cols-4 lg:grid-cols-5  max-h-[calc(100vh - 180px)] overflow-y-auto">
              {cData?.getCustomer.cart?.products.map(({ meta }) => {
                let {
                  id,
                  name,
                  sale_start,
                  quantity,
                  image,
                  offer_price,
                  price,
                } = meta;

                let data = {
                  node: {
                    id,
                    name,
                    sale_start,
                    quantity,
                    image,
                    offer_price,
                    price,
                  },
                };

                return (
                  <Product
                    key={data?.node?.id}
                    hit={data?.node}
                    customer={cData?.getCustomer}
                    error={cError}
                    refresh={reexecuteQuery}
                  />
                );
              })}
            </div>

            <div>
              {cData?.getCustomer?.awaitingPacking.length > 0 && (
                <>
                  <Divider orientation="right">Unpacked</Divider>
                  <div className="flex p-2  max-w-[calc(100vw-32px)] overflow-y-auto ">
                    {cData?.getCustomer?.awaitingPacking.map((order, i) => (
                      <PList order={order} key={i} />
                    ))}
                  </div>
                </>
              )}

              {cData?.getCustomer?.packed.length > 0 && (
                <>
                  <Divider orientation="right">Packed</Divider>
                  <div className="flex p-2  max-w-[calc(100vw-32px)] overflow-y-auto ">
                    {cData?.getCustomer?.packed.map((order, i) => (
                      <PList order={order} key={i} />
                    ))}
                  </div>
                </>
              )}

              {cData?.getCustomer?.old.length > 0 && (
                <>
                  <Divider orientation="right">Old orders</Divider>
                  <div className="flex p-2  max-w-[calc(100vw-32px)] overflow-y-auto ">
                    {cData?.getCustomer?.old.map((order, i) => (
                      <PList order={order} key={i} />
                    ))}
                  </div>
                </>
              )}
            </div>

            <Button
              onClick={() => router.push("/checkout")}
              rounded
              label="Checkout"
              yellow
              width="90%"
              large
              style={{ position: "fixed", bottom: "1rem" }}
            />
          </div>
        )}

        {(cData?.getCustomer?.cart?.products.length == 0 ||
          !cData?.getCustomer?.cart) && (
          <div style={{ maxHeight: "calc(100vh - 170px)" }}>
            <div style={{ height: "70vh" }}>
              <img
                src="https://img.icons8.com/dusk/100/000000/shopping-basket-2.png"
                style={{
                  position: "absolute",
                  top: "20vh",
                  left: "50%",
                  transform: "translateX(calc(-50% + 8px))",
                }}
              />
              <p
                style={{
                  color: "#3F9B42",
                  position: "absolute",
                  transform: "translateX(calc(-50% + 8px))",
                  letterSpacing: "-0.05rem",
                  top: "calc(20vh + 120px)",
                  left: "50%",
                  fontSize: "1.5rem",
                }}
              >
                Basket empty!
              </p>
              <ButtonAntd
                style={{
                  width: "100%",
                  color: "black",
                  position: "absolute",
                  transform: "translateX(calc(-50% + 8px))",
                  top: "calc(20vh + 180px)",
                  left: "50%",
                }}
                onClick={() => router.push("/")}
                type="link"
              >
                <p style={{ textDecoration: "underline", fontWeight: "400" }}>
                  {" "}
                  Start shopping
                </p>
              </ButtonAntd>
            </div>
            <div>
              {cData?.getCustomer?.awaitingPacking.length > 0 && (
                <>
                  <Divider orientation="right">Unpacked</Divider>
                  <div className="flex p-2  max-w-[calc(100vw-32px)] overflow-y-auto ">
                    {cData?.getCustomer?.awaitingPacking.map((order, i) => (
                      <PList order={order} key={i} />
                    ))}
                  </div>
                </>
              )}
              {cData?.getCustomer?.packed.length > 0 && (
                <>
                  <Divider orientation="right">Packed</Divider>
                  <div className="flex p-2  max-w-[calc(100vw-32px)] overflow-y-auto ">
                    {cData?.getCustomer?.packed.map((order, i) => (
                      <PList order={order} key={i} />
                    ))}
                  </div>
                </>
              )}
              {cData?.getCustomer?.old.length > 0 && (
                <>
                  <Divider orientation="right">Old orders</Divider>
                  <div className="flex p-2  max-w-[calc(100vw-32px)] overflow-y-auto ">
                    {cData?.getCustomer?.old.map((order, i) => (
                      <PList order={order} key={i} />
                    ))}
                  </div>
                </>
              )}
            </div>
          </div>
        )}
      </Container>
    </Page>
  );
}

const MProduct = ({ data }) => {
  return (
    <div
      id={data?.meta?.id}
      style={{ display: "inline" }}
      className="bg-[#f1f1f1] relative rounded-md mr-4 mb-4 max-w-[200px]"
    >
      <div style={{ padding: 12 }}>
        <Image
          style={{
            width: `calc(50vw - 56px)`,
            height: `calc(50vw - 56px)`,
            objectFit: "cover",
            marginBottom: 12,
            width: "100%",
            maxWidth: "calc(200px - 24px)",
            maxHeight: "calc(200px - 24px)",
          }}
          src={data?.meta?.image ? data?.meta?.image : "/icon-192x192.png"}
          loading="eager"
        />

        <Tooltip title={data?.meta?.name} color="#3F9B42" trigger="click">
          <Paragraph
            ellipsis={{ rows: 2, expandable: false }}
            style={{
              color: "#707070",
              width: `calc(50vw - 60px)`,
              maxWidth: "calc(200px - 24px)",
            }}
          >
            {data?.meta?.name}
          </Paragraph>
        </Tooltip>

        <Title text={`Ksh. ${data?.meta?.price}, ${data?.quantity} item`} />

        {data?.disburseDate && (
          <p>
            {moment(new Date(parseInt(data?.disburseDate))).format(
              `Do MMM | h: mm a`
            )}
          </p>
        )}
      </div>
    </div>
  );
};

const PList = ({ order }) => {
  return (
    <div className="flex">
      {order.products.map((product, i) => {
        let data = {
          meta: {
            id: product.meta.id,
            image: product.meta.image,
            name: product.meta.name,
            price: product.meta.price,
          },
          quantity: product.quantity,
          disburseDate: order.disburseDate,
        };

        console.log(data);

        return <MProduct data={data} key={i} />;
      })}
    </div>
  );
};
