import { Skeleton } from "antd";

export default function SkeletonCustom(key) {
  return (
    <div
      style={{
        width: "calc(50vw - 32px)",
        margin: "0px 16px 16px 5px",
        background: "#f1f1f1",
        borderRadius: "8px",
        position: "relative",
      }}
    >
      <div style={{ padding: 12 }}>
        <Skeleton.Button
          style={{
            width: `calc(50vw - 56px)`,
            height: `calc(50vw - 56px)`,
            marginBottom: 8,
          }}
          active
        />
        <br />
        <Skeleton.Button
          style={{ width: `calc(50vw - 56px)`, marginBottom: 8 }}
          active
          size="small"
        />
        <Skeleton.Button style={{ width: 100, marginBottom: 16 }} active />
        <Skeleton.Button
          style={{
            width: `calc(50vw - 56px)`,
            height: "2.1rem",
            marginBottom: 2,
          }}
          active
        />
      </div>
    </div>
  );
}
