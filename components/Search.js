import { SearchOutlined } from "@ant-design/icons";

export default function Search({
  placeholder,
  onChange,
  value,
  style,
  rounded,
}) {
  return (
    <row
      style={{
        width: "calc(100% - 130px)",
        backgroundColor: "#f1f1f1",
        borderRadius: rounded ? "6px" : "0px",
      }}
    >
      <input
        value={value}
        style={{
          ...style,
          padding: "16px",
          outline: "none",
          border: "none",
          background: "transparent",
          width: "90%",
        }}
        placeholder={placeholder}
        onChange={onChange}
      />
      <SearchOutlined />
    </row>
  );
}
