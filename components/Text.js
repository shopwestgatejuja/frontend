export default function Text({ text, center, style, small }) {
  return (
    <p
      style={{
        ...style,
        fontSize: small ? "0.7rem" : "0.9rem",
        color: "#707070",
        fontFamily: "Metropolis-Regular",
        display: "block",
        width: "100%",
        margin: 0,
        textAlign: center ? "center" : "left",
      }}
    >
      {text}
    </p>
  );
}
