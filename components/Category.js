import { Typography } from "antd";

const { Text } = Typography;

export default function Category({ icon, label, rounded, onSelect, active }) {
  return (
    <div
      style={{
        width: "calc((100vw - 76px )/ 5)",
        maxWidth: 100,
        marginRight: 12,
        marginBottom: 12,
      }}
    >
      <button
        onClick={onSelect}
        className={
          active
            ? `outline outline-offset-2 outline-4 outline-green-500`
            : undefined
        }
        style={{
          width: "calc((100vw - 76px )/ 5)",
          maxWidth: 100,
          height: "calc((100vw - 76px )/ 5)",
          maxHeight: 100,
          background: "#f1f1f1",
          borderRadius: rounded ? "6px" : "0px",
          justifyContent: "center",
          alignItems: "center",
          border: "none",
          position: "relative",
        }}
      >
        <img
          src={icon}
          alt="cat"
          className="translate-x-[-50%] translate-y-[-50%]"
          style={{
            width: "calc((100vw - 250px )/ 5)",
            maxWidth: 60,
            maxHeight: 60,
            position: "absolute",
            top: "50%",
            left: "50%",
            height: "calc((100vw - 250px )/ 5)",
            objectFit: "cover",
          }}
        />
      </button>

      <Text
        ellipsis
        style={{
          width: "calc((100vw - 76px )/ 5)",
          color: "#707070",
          width: "100%",
          fontFamily: "Metropolis-Regular",
          textAlign: "center",
          fontSize: "0.7rem",
          margin: 0,
          marginTop: 4,
        }}
      >
        {label}
      </Text>
    </div>
  );
}
