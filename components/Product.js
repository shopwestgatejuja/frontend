import Title from "./Title";
import Button from "./Button";
import {
  Image,
  Button as ButtonAntd,
  Tag,
  Typography,
  Tooltip,
  message,
} from "antd";

import { useState } from "react";
import { useRouter } from "next/router";
import { useMutation } from "urql";
import { FaPlus, FaMinus } from "react-icons/fa";

const { Paragraph } = Typography;

export default function Product({ customer, hit, error, refresh }) {
  // States
  const [loading, setLoading] = useState(false);
  const router = useRouter();
  const [IB, setIB] = useState(false);
  const [DB, setDB] = useState(false);

  // Requests
  const UPDATE_CART = `
    mutation UPDATE_CART(
      $action: String!
      $prod_id: ID!
      $customer: ID!
    ){
      updateCart(
        action: $action
        prod_id: $prod_id
        customer: $customer
      ){
        id
      }
    }
  `;

  const ADD_TO_CART = `
  mutation ADD_TO_CART(
    $quantity: Int
    $prod_id: ID!
    $customer: ID!
  ){
    addToCart(
      quantity: $quantity
      prod_id: $prod_id
      customer: $customer
    ){
      id
    }
  }
`;

  const [req1Result, _updateCart] = useMutation(UPDATE_CART);
  const [req2Result, _addToCart] = useMutation(ADD_TO_CART);

  // Functions
  const removeFromCart = () => {
    message.info("Hold on");
    _updateCart({
      action: "remove",
      prod_id: hit?.id,
      customer: customer?.id,
    }).then(({ data: _data }) => {
      if (_data) {
        message.success(`'${hit?.name}' removed from cart`);
        refresh();
        return;
      }
      message.error("Oh no... server error");
    });
  };

  const increment = () => {
    setIB(true);
    message.info("Hold on");
    _updateCart({
      action: "increment",
      prod_id: hit?.id,
      customer: customer?.id,
    }).then(({ data: _data }) => {
      if (_data) {
        refresh();
        return;
      }
      message.error("Oh no... server error");
    });
    setIB(false);
  };

  const decrement = () => {
    message.info("Hold on");
    setDB(true);
    if (
      customer?.cart?.products.filter(
        (product) => product?.meta?.id == hit?.id
      )[0].quantity == 1
    ) {
      removeFromCart();
      return;
    }
    _updateCart({
      action: "decrement",
      prod_id: hit?.id,
      customer: customer?.id,
    }).then(({ data: _data }) => {
      if (_data) {
        refresh();
        return;
      }
      message.error("Oh no... server error");
    });
    setDB(false);
  };

  const addToCart = () => {
    if (error) {
      router.push("/login");
      return;
    }
    setLoading(true);
    _addToCart({
      quantity: 1,
      prod_id: hit?.id,
      customer: customer?.id,
    }).then(({ data: _data }) => {
      if (_data?.addToCart) {
        setLoading(false);
        message.success(`'${hit?.name}' added to cart`);
        refresh();
        return;
      }
      message.error("Oh no... server error");
    });
  };

  return (
    <section
      id={hit?.id}
      className="relative col-span-1 bg-gray-50 p-4 w-full rounded"
      // style={{
      //   width: "calc(50vw - 32px)",
      //   maxWidth: 200,
      //   margin: "0px 16px 16px 5px",
      //   background: "#f1f1f1",
      //   borderRadius: "8px",
      //   position: "relative",
      // }}
    >
      {customer?.cart?.products.filter(
        (product) => product?.meta?.id == hit?.id
      ).length > 0 && (
        <Button
          onClick={removeFromCart}
          label={
            <img
              src={"/cross.svg"}
              style={{ height: 32, width: 32, objectFit: "cover", zIndex: -2 }}
            />
          }
          style={{
            position: "absolute",
            zIndex: 2,
            top: -12,
            right: -12,
            height: 32,
            background: "transparent",
          }}
          width={32}
        />
      )}

      {Date.now() > parseInt(hit?.sale_start) &&
        Date.now() < parseInt(hit?.sale_end) && (
          <Tag
            color="green"
            style={{
              zIndex: 2,
              position: "absolute",
              top: 0,
              left: 0,
            }}
          >
            OFFER!
          </Tag>
        )}

      {hit?.outstocked && (
        <Tag
          color="red"
          style={{
            zIndex: 2,
            position: "absolute",
            top: 12,
            left: 0,
          }}
        >
          Outstocked
        </Tag>
      )}

      <div style={{ padding: 12 }}>
        <Image
          style={{
            width: `calc(50vw - 56px)`,
            height: `calc(50vw - 56px)`,
            objectFit: "cover",
            marginBottom: 12,
            zIndex: -1,
            width: "100%",
            maxWidth: "calc(200px - 24px)",
            maxHeight: "calc(200px - 24px)",
          }}
          src={hit?.image ? hit?.image : "/icon-192x192.png"}
          loading="eager"
        />

        <Tooltip title={hit?.name} color="#3F9B42" trigger="click">
          <Paragraph
            ellipsis={{ rows: 2, expandable: false }}
            style={{
              color: "#707070",
              width: `calc(50vw - 60px)`,
              maxWidth: "calc(200px - 24px)",
              height: 46,
            }}
          >
            {hit?.name}
          </Paragraph>
        </Tooltip>

        {Date.now() > parseInt(hit?.sale_start) &&
          Date.now() < parseInt(hit?.sale_end) && (
            <span
              style={{
                display: "flex",
                margin: "0.4rem 0rem",
                display: "flex",
                justifyContent: "space-between",
                width: "100%",
              }}
            >
              <Title text={`Ksh. ${hit?.offer_price}`} style={{ margin: 0 }} />
              <h3
                style={{
                  textDecoration: "line-through",
                  color: "gray",
                  margin: 0,
                  lineHeight: 2,
                  fontSize: "0.8rem",
                  fontWeight: "400",
                }}
              >
                {hit?.price}
              </h3>
            </span>
          )}

        {(Date.now() < parseInt(hit?.sale_start) ||
          Date.now() > parseInt(hit?.sale_end) ||
          !hit?.sale_start ||
          !hit?.sale_end) && <Title text={`Ksh. ${hit?.price}`} />}

        {customer?.cart?.products.filter(
          (product) => product?.meta?.id == hit?.id
        ).length > 0 && (
          <div className="flex justify-between w-full">
            <ButtonAntd
              style={{
                background: "red",
                height: 36,
                width: 36,
                borderRadius: 8,
                background: "#3F9B42",
                alignItems: "center",
                justifyItems: "center",
              }}
              loading={DB}
              onClick={decrement}
            >
              <FaMinus color="white" />
            </ButtonAntd>

            <Title
              style={{
                lineHeight: 1,
                color: "#707070",
                width: "100%",
                textAlign: "center",
              }}
              text={
                customer?.cart?.products.filter(
                  (product) => product?.meta?.id == hit?.id
                )[0].quantity
              }
            />
            <ButtonAntd
              style={{
                background: "red",
                height: 36,
                width: 36,
                borderRadius: 8,
                background: "#3F9B42",
              }}
              loading={IB}
              className="h-[36px] w-[36px]"
              onClick={increment}
            >
              <FaPlus color="white" />
            </ButtonAntd>
          </div>
        )}

        {(customer?.cart?.products.filter(
          (product) => product?.meta?.id == hit?.id
        ).length <= 0 ||
          !customer?.cart) && (
          <ButtonAntd
            loading={loading}
            onClick={addToCart}
            style={{
              fontFamily: "Metropolis-Regular",
              display: "block",
              fontSize: "1rem",

              backgroundColor: "#3F9B42",
              fontWeight: "900",
              textTransform: "uppercase",
              color: "#fff",
              border: "none",
              borderRadius: "8px",
              width: "100%",
              height: 36,
            }}
            full
          >
            {loading ? "Adding" : "Add"}
          </ButtonAntd>
        )}
      </div>
    </section>
  );
}
