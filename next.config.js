const withAntdLess = require("next-plugin-antd-less");
const withPlugins = require("next-compose-plugins");

// optional next.js configuration
const nextConfig = {
  env: {
    API_KEY: "eWeNo2msOp0",
  },
  reactStrictMode: true,
  webpack5: true,
  webpack: (config) => {
    config.resolve.fallback = { fs: false };
    return config;
  },
};

module.exports = withPlugins(
  [
    // add a plugin with specific configuration

    // another plugin with a configuration
    [
      withAntdLess,
      {
        // optional: you can modify antd less variables directly here
        modifyVars: { "@primary-color": "#3F9B42" },
        // Or better still you can specify a path to a file
        // lessVarsFilePath: "./styles/variables.less",
        // optional
        lessVarsFilePathAppendToEndOfContent: false,
        // optional https://github.com/webpack-contrib/css-loader#object
        cssLoaderOptions: {},
      },
    ],
  ],
  nextConfig
);
